val MAX_ROLLS: Int = 3   //players can roll the dices three times and observe the end result

fun main(){
    var player1 = Player();
    var player2 = Player();
    var i: Int=0;
    while(i++<2){  //the players should be in a bigger while loop but makes no sense 'cause no points tracking is implemented
        println("**********************************************************")
        println("*                       Player One                       *")
        println("**********************************************************")
        player1.play()
        println("\n\n")
        println("**********************************************************")
        println("*                       Player Two                       *")
        println("**********************************************************")
        player2.play()
        println("\n\n")
    }
}

class Dice(
    var number: Int = 0,          //the value of the dice
    var locked: Boolean = false   //locked or not (if locked the dice cant be rolled)
    ){
        fun roll(): Int{
            if(!this.locked)
                this.number = (1..6).random()  //get a "random" number between 1 and 6
            return this.number                 //return it 'cause if rolled we always have to get the value anyway
        }
        fun lock(){
            this.locked = true      //lock
        }
        fun unlock(){
            this.locked = false     //unlock
        }
        fun getValue(): Int{
            return this.number     //get the value
        }
        fun isLocked(): Boolean{
            return this.locked     //check if the dice is locked
        }
}

class Player(                   //a player rolls 6 dices
    var dice1: Dice = Dice(),
    var dice2: Dice = Dice(),
    var dice3: Dice = Dice(),
    var dice4: Dice = Dice(),
    var dice5: Dice = Dice(),
    var dice6: Dice = Dice()
    ){
        val dices = arrayOf<Dice>(dice1, dice2, dice3, dice4, dice5, dice6)  //put it into an array to access it more easily
        var one_cnt: Int = 0      //counts of rolled numbers so poker, jamb, etc. can be checked 
        var two_cnt: Int = 0
        var three_cnt: Int = 0
        var four_cnt: Int = 0
        var five_cnt: Int = 0
        var six_cnt: Int = 0
        val counter = arrayOf<Int>(one_cnt, two_cnt, three_cnt, four_cnt, five_cnt, six_cnt)  //array for easy access
        fun play(){         
            var numLocked: Int = 0   //if all six dices are locked there is no need to play the rest of the 3 rolls
            var read: String         //user input
            for( i in 1..MAX_ROLLS){
                if (i==1) println("\nFirst roll...") else if(i==2) println("\nSecond roll...") else println("\nThird roll...")
                for( j in 0..dices.size-1){  //roll eeach dice, print the values and update the counter
                    if(dices[j].isLocked()) println("  ${j+1}. ${dices[j].roll()}\tlocked") else println("  ${j+1}. ${dices[j].roll()}")
                    if (dices[j].getValue() == 1) counter[0]++
                    else if (dices[j].getValue() == 2) counter[1]++
                    else if (dices[j].getValue() == 3) counter[2]++
                    else if (dices[j].getValue() == 4) counter[3]++
                    else if (dices[j].getValue() == 5) counter[4]++
                    else if (dices[j].getValue() == 6) counter[5]++
                }
                for (j in 0..counter.size-1) { 
                    if (counter[j] == 6)
                        println("\n------JAMB!! Congrats------\n")  //if a number was counter 6 times it's a jamb
                    else if (counter[j] == 5)
                        println("\n------POKER! Congrats------\n")  //if a number was counted 5 times it's a poker
                }
                //if each number appears once it's a scale
                if (counter[0]==1 && counter[1]==1 && counter[2]==1 && counter[3]==1 && counter[4]==1 && counter[5]==1) println("\n------SCALE! Congrats------\n")
                for (j in 0..counter.size-1) counter[j] = 0   //reset the counter
                if(i!=3){
                    println("Do you want to lock any dices? (y/n)")
                    read = readLine()!!
                    if(read=="y"){
                        println("Enter the dice #'s one after another and type 'x' when you're done")
                        read = readLine()!!
                        while(read!="x"){       //if the user inputs x, the locking is done
                            if (read == "1"){
                                dices[0].lock()
                                numLocked++}
                            else if (read == "2"){
                                dices[1].lock() 
                                numLocked++}
                            else if (read == "3") {
                                dices[2].lock()
                                numLocked++}
                            else if (read == "4") {
                                dices[3].lock()
                                numLocked++}
                            else if (read == "5") {
                                dices[4].lock()
                                numLocked++}
                            else if (read == "6") {
                                dices[5].lock()
                                numLocked++}
                            read=readLine()!!
                        }
                    }
                    if (numLocked==6){               //if all six dices are locked the player can't play this round no more
                        for( j in 0..dices.size-1){
                            dices[j].unlock()        //unlock for the next round
                        }
                        return
                    }
                }
            }
            for( j in 0..dices.size-1){
                dices[j].unlock()        //unlock for the next round
            }
        }
}